
enum class Rainbow(val color: String) {
    vermell("vermell"),
    taronja("taronja"),
    groc("groc"),
    verd("verd"),
    cian("cian"),
    blau("blau"),
    violeta("violeta")
}


fun main(){

    print("Introduzca uno de los colores del arco de San Martí: ")
    val color_input = scan.next().toLowerCase()
    val color_list = mutableListOf<String>()

    for (i in Rainbow.values()){
        color_list.add(i.toString().toLowerCase())
    }
    if (color_input in color_list){ println("¡El color ${color_input.toUpperCase()} está dentro del arcoíris de San Martí!") }
    else{ println("¡El color ${color_input.toUpperCase()} no está dentro del arcoíris de San Martí!") }

    /* Esta es la versión con el fichero que hice desde un inicio
    val rainbowcolors = File("./src/main/resources/ex2colors.txt")
    val readcolors = rainbowcolors.readLines()
    val color_input = scan.next().toLowerCase()
    [Aquí iría un "for" simple]
    val color_list = mutableListOf<String>()
    if (color_input in color_list){ println("¡El color $color_input está dentro del arcoíris de San Martí!") }
    else{ println("¡El color $color_input no está dentro del arcoíris de San Martí!") }
     */
}
