import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
data class Student(
        var name : String,
        var textGrade : String
) : java.io.Serializable

fun main(){
    val studentdata = File("ex1estudiantdata.json")
    //============================================================================
    // AÑADIR DATOS DE ALUMNO
    for (add in 1..2){
        print("Introduzca nombre de alumno $add: ")
        val studentname = scan.next()
        var studentgrade = ""

        while (studentgrade != "suspes" && studentgrade != "aprovat" && studentgrade != "be" && studentgrade != "notable" && studentgrade != "excellent") {
            print("Introduzca su nota[suspes, aprovat, be, notable, excellent]: ")

            studentgrade = scan.next().toLowerCase()

            if (studentgrade != "suspes" && studentgrade != "aprovat" && studentgrade != "be" && studentgrade != "notable" && studentgrade != "excellent") {
                println("Introduzca una nota válida.")
            }
        }
        if (studentgrade == "suspes"){studentgrade = "failed"}
        val new_student = Student(name = studentname, textGrade = studentgrade.toUpperCase())
        val studentobject = Json.encodeToString(new_student)
        studentdata.appendText("$studentobject\n")
    }
    //============================================================================

    //MOSTRAR DATOS GUARDADOS DEL ALUMNO

    println("===================================================================")
    val readstudentdata = studentdata.readLines()
    val studentlist = mutableListOf<Student>()

    for (i in readstudentdata.iterator()){
        studentlist.add(Json.decodeFromString(i))
    }
    studentlist.forEach(){ println(it)}
    //============================================================================

}