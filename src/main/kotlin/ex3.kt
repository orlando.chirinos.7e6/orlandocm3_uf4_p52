import java.util.Scanner

val scan = Scanner(System.`in`)

open class Instrument() {
    open fun makeSounds(i: Int) {
    }
}

class Triangle(val X: Int):Instrument(){
    override fun makeSounds(i: Int){
        repeat(i){
            println("T${"I".repeat(X)}NC")
        }
    }
}

class Drum(val Y: String):Instrument(){
    override fun makeSounds(i:Int){
        repeat(i){
            when(Y){
                "A" -> println("TAAAAM")
                "O" -> println("TOOOOM")
                "U" -> println("TUUUUM")
            }
        }
    }
}

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
            Triangle(5),
            Drum("A"),
            Drum("O"),
            Triangle(1),
            Triangle(5)
    )
    play(instruments)
}
private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}
